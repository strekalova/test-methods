package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 29.09.2016.
 */
public class InvalidPasswordCustomerException extends IllegalArgumentException {

    public final static String PW_LEN_LESS = "Password length must be greater than ";
    public final static String PW_LEN_GRET = "Password length must be lesser than ";
    public final static String PW_MTCH_LOGIN = "Password must not contain login.";
    public final static String PW_MTCH_FN = "Password must not contain first name.";
    public final static String PW_MTCH_LN = "Password must not contain last name.";
    public final static String PW_EASY = "Password too easy!";

    public InvalidPasswordCustomerException() { super(); }

    public InvalidPasswordCustomerException(String msg) { super(msg); }
}
