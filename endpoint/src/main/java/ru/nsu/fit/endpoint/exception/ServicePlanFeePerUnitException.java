package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class ServicePlanFeePerUnitException extends IllegalArgumentException
{
    public static final String MIN_VALUE = "Value in field \"FeePerUnit\" must be greater than or equal to ";
    public static final String MAX_VALUE = "Value in field \"FeePerUnit\" must be less than or equal to ";

    public ServicePlanFeePerUnitException(String s)
    {
        super(s);
    }
}
