package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.exception.*;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class ServicePlan {
    private UUID id;

    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    public static final int nameMaxLength = 128;
    public static final int nameMinLength = 2;
    private String name;

    /* Длина не больше 1024 символов и не меньше 1 включительно */
    public static final int detailsMinLength = 1;
    public static final int detailsMaxLength = 1024;
    private String details;

    /* Не больше 999999 и не меньше 1 включительно */
    public static final int maxSeatsMin = 1;
    public static final int maxSeatsMax = 999999;
    private int maxSeats;

    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    public static final int minSeatsMin = 1;
    public static final int minSeatsMax = 999999;
    private int minSeats;

    /* Больше ли равно 0 но меньше либо равно 999999 */
    public static final int minFeePerUnit = 0;
    public static final int maxFeePerUnit = 999999;
    private int feePerUnit;

    public ServicePlan(String name, String details, int maxSeats, int minSeats, int feePerUnit)
    {
        validate(name, details, maxSeats, minSeats, feePerUnit);
        this.id = UUID.randomUUID();
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;
    }

    private static void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit)
    {
        validateName(name);
        validateDetails(details);
        validateMaxSeats(maxSeats);
        validateMinSeats(minSeats, maxSeats);
        validateFeePerUnit(feePerUnit);
    }

    private static void validateFeePerUnit(int feePerUnit)
    {
        if (feePerUnit < minFeePerUnit)
            throw new ServicePlanFeePerUnitException(ServicePlanFeePerUnitException.MIN_VALUE + minFeePerUnit);
        if (feePerUnit > maxFeePerUnit)
            throw new ServicePlanFeePerUnitException(ServicePlanFeePerUnitException.MAX_VALUE + maxFeePerUnit);
    }

    private static void validateMinSeats(int minSeats, int maxSeats)
    {
        if (minSeats < minSeatsMin)
            throw new ServicePlanMinSeatsException(ServicePlanMinSeatsException.MIN_VALUE + minSeatsMin);
        if (minSeats > minSeatsMax)
            throw new ServicePlanMinSeatsException(ServicePlanMinSeatsException.MAX_VALUE + minSeatsMax);
        if (minSeats > maxSeats)
            throw new ServicePlanMinSeatsException(ServicePlanMinSeatsException.MAX_SEATS_CONSTRAINT);
    }

    private static void validateMaxSeats(int maxSeats)
    {
        if (maxSeats < maxSeatsMin)
            throw new ServicePlanMaxSeatsException(ServicePlanMaxSeatsException.MIN_VALUE + maxSeatsMin);
        if (maxSeats > maxSeatsMax)
            throw new ServicePlanMaxSeatsException(ServicePlanMaxSeatsException.MAX_VALUE + maxSeatsMax);
    }

    private static void validateDetails(String details)
    {
        if (details.length() > detailsMaxLength)
            throw new ServicePlanDetailsException(ServicePlanDetailsException.MAX_LEN + detailsMaxLength);
        if (details.length() < detailsMinLength)
            throw new ServicePlanDetailsException(ServicePlanDetailsException.MIN_LEN + detailsMinLength);
    }

    private static void validateName(String name)
    {
        if (name.length() > nameMaxLength)
            throw new ServicePlanNameException(ServicePlanNameException.MAX_LEN + nameMaxLength);
        if (name.length() < nameMinLength)
            throw new ServicePlanNameException(ServicePlanNameException.MIN_LEN + nameMinLength);
        for (char c : name.toCharArray())
        {
            if (!Character.isLetterOrDigit(c))
                throw new ServicePlanNameException(ServicePlanNameException.SYMBOLS);
        }
    }
}
