package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.exception.SubscriptionMaxSeatsException;
import ru.nsu.fit.endpoint.exception.SubscriptionMinSeatsException;
import ru.nsu.fit.endpoint.exception.SubscriptionUsedSeatsException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {



    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;

    public static final int maxSeatsMin = 1;
    public static final int maxSeatsMax = 999999;
    private int maxSeats;

    public static final int minSeatsMin = 1;
    public static final int minSeatsMax = 999999;
    private int minSeats;

    private int usedSeats;

    public Subscription(int maxSeats, int minSeats, int usedSeats)
    {
        validate(maxSeats, minSeats, usedSeats);
        this.id = UUID.randomUUID();
        this.customerId = UUID.randomUUID();
        this.servicePlanId = UUID.randomUUID();
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.usedSeats = usedSeats;
    }

    private static void validate(int maxSeats, int minSeats, int usedSeats)
    {
        validateMaxSeats(maxSeats);
        validateMinSeats(minSeats, maxSeats);
        validateUsedSeats(usedSeats, minSeats, maxSeats);
    }

    private static void validateMinSeats(int minSeats, int maxSeats)
    {
        if (minSeats < minSeatsMin)
            throw new SubscriptionMinSeatsException(SubscriptionMinSeatsException.MIN_VALUE + minSeatsMin);
        if (minSeats > minSeatsMax)
            throw new SubscriptionMinSeatsException(SubscriptionMinSeatsException.MAX_VALUE + minSeatsMax);
        if (minSeats > maxSeats)
            throw new SubscriptionMinSeatsException(SubscriptionMinSeatsException.MAX_SEATS_CONSTRAINT);
    }

    private static void validateMaxSeats(int maxSeats)
    {
        if (maxSeats < maxSeatsMin)
            throw new SubscriptionMaxSeatsException(SubscriptionMaxSeatsException.MIN_VALUE + maxSeatsMin);
        if (maxSeats > maxSeatsMax)
            throw new SubscriptionMaxSeatsException(SubscriptionMaxSeatsException.MAX_VALUE + maxSeatsMax);
    }

    private static void validateUsedSeats(int usedSeats, int minSeats, int maxSeats)
    {
        if (usedSeats < minSeats)
            throw new SubscriptionUsedSeatsException(SubscriptionUsedSeatsException.MIN_SEATS_CONSTRAINT);
        if (usedSeats > maxSeats)
            throw new SubscriptionUsedSeatsException(SubscriptionUsedSeatsException.MAX_SEATS_CONSTRAINT);
    }
}
