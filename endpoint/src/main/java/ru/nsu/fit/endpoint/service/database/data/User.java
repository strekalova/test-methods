package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.exception.InvalidEmailUserException;
import ru.nsu.fit.endpoint.exception.InvalidFirstNameUserException;
import ru.nsu.fit.endpoint.exception.InvalidLastNameUserException;
import ru.nsu.fit.endpoint.exception.InvalidPasswordUserException;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;

    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    public final static int MIN_FN_LEN = 2;
    public final static int MAX_FN_LEN = 12;
    private String firstName;

    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    public final static int MIN_LN_LEN = 2;
    public final static int MAX_LN_LEN = 12;
    private String lastName;

    /* указывается в виде email, проверить email на корректность */
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private String login;

    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    public final static int MIN_PW_LEN = 6;
    public final static int MAX_PW_LEN = 12;
    private final static String SPEC_SYMS = "!@#$%^&*_+=-";
    private String pass;

    private UserRole userRole;

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public User(String firstName, String lastName, String login, String pass)
    {
        validate(firstName, lastName, login, pass);
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
    }

    public static void validate(String firstName, String lastName, String login, String pass)
    {
        validateLogin(login);
        validateFirstName(firstName);
        validateLastName(lastName);
        validatePassword(pass, login, firstName, lastName);
    }

    private static void validatePassword(String pass, String login, String firstName, String lastName) {
        if (pass.length() < User.MIN_PW_LEN)
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_LEN_LESS + MIN_PW_LEN);
        if (pass.length() > User.MAX_PW_LEN)
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_LEN_GRET + MAX_PW_LEN);

        if (pass.toUpperCase().contains(login.toUpperCase()) || pass.toUpperCase().contains(login.toUpperCase().substring(0, login.lastIndexOf('@'))))
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_MTCH_LOGIN);
        if (pass.toUpperCase().contains(firstName.toUpperCase()))
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_MTCH_FN);
        if (pass.toUpperCase().contains(lastName.toUpperCase()))
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_MTCH_LN);

        boolean digitFlag = false, symFlag= false, upCaseSymFlag = false, specSymFlag = false, otherSymFlag = true;
        for (int i = 0; i < pass.length(); i++)
        {
            char c = pass.charAt(i);
            if (Character.isDigit(c))
            {
                digitFlag = true;
            }
            else
            if (Character.isLetter(c))
            {
                if (Character.isLowerCase(c))
                    symFlag = true;
                else
                    upCaseSymFlag = true;
            }
            else
            if (SPEC_SYMS.contains(c + ""))
                specSymFlag = true;
            else
                otherSymFlag = false;
        }

        if (!(digitFlag && symFlag && upCaseSymFlag && specSymFlag && otherSymFlag))
        {
            throw new InvalidPasswordUserException(InvalidPasswordUserException.PW_EASY);
        }
    }

    private static void validateLogin(String login) {
        if (!(VALID_EMAIL_ADDRESS_REGEX .matcher(login).find()))
        {
            throw new InvalidEmailUserException(InvalidEmailUserException.MSG);
        }
    }

    private static void validateLastName(String lastName) {
        if (lastName.length() < MIN_LN_LEN)
        {
            throw new InvalidLastNameUserException(InvalidLastNameUserException.LN_LEN_LESS + MIN_LN_LEN);
        }
        if (lastName.length() > MAX_LN_LEN)
        {
            throw new InvalidLastNameUserException(InvalidLastNameUserException.LN_LEN_GRET + MAX_LN_LEN);
        }
        if (!lastName.matches("^[A-Z][a-z]*$"))
        {
            throw new InvalidLastNameUserException(InvalidLastNameUserException.LN_RULES);
        }
    }

    private static void validateFirstName(String firstName) {
        if (firstName.length() < MIN_FN_LEN)
        {
            throw new InvalidFirstNameUserException(InvalidFirstNameUserException.FN_LEN_LESS + MIN_FN_LEN);
        }
        if (firstName.length() > MAX_FN_LEN)
        {
            throw new InvalidFirstNameUserException(InvalidFirstNameUserException.FN_LEN_GRET + MAX_FN_LEN);
        }
        if (!firstName.matches("^[A-Z][a-z]*$"))
        {
            throw new InvalidFirstNameUserException(InvalidFirstNameUserException.FN_RULES);
        }
    }

}
