package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    private String okFirstName = "Jessica";
    private String okLastName = "Jones";
    private String okLogin = "jessy_J@gmail.com";
    private String okPass = "caGe16&";
    private int okMoney = 100;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomerWithShortFirstName()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_LEN_LESS);
        String shortName = "A";
        for (int i = 2; i < Customer.MIN_FN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new Customer(shortName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkShortFirstName()
    {
        expectedEx = ExpectedException.none();
        String shortName = "A";
        for (int i = 2; i <= Customer.MIN_FN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new Customer(shortName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkLongFirstName()
    {
        expectedEx = ExpectedException.none();
        String longName = "A";
        for (int i = 2; i <= Customer.MAX_FN_LEN; i++)
        {
            longName = longName + "a";
        }
        new Customer(longName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLongFirstName()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_LEN_GRET);
        String longName = "A";
        for (int i = 2; i <= Customer.MAX_FN_LEN + 1; i++)
        {
            longName = longName + "a";
        }
        new Customer(longName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithFirstNameWithoutUppercaseFirstLetter()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_RULES);
        String noUpperName = "claire";
        new Customer(noUpperName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithFirstNameWithUppercaseLetters()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_RULES);
        String upperName = "PETER";
        new Customer(upperName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithFirstNameWithDigits()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_RULES);
        String digitName = "Nick007";
        new Customer(digitName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithFirstNameWithSymbols()
    {
        expectedEx.expect(InvalidFirstNameCustomerException.class);
        expectedEx.expectMessage(InvalidFirstNameCustomerException.FN_RULES);
        String symbolName = "Nicole^_^";
        new Customer(symbolName, okLastName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithShortLastName()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_LEN_LESS);
        String shortName = "A";
        for (int i = 2; i < Customer.MIN_LN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new Customer(okFirstName, shortName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkShortLastName()
    {
        expectedEx = ExpectedException.none();
        String shortName = "A";
        for (int i = 2; i <= Customer.MIN_LN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new Customer(okFirstName, shortName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkLongLastName()
    {
        expectedEx = ExpectedException.none();
        String longName = "A";
        for (int i = 2; i <= Customer.MAX_LN_LEN; i++)
        {
            longName = longName + "a";
        }
        new Customer(okFirstName, longName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLongLastName()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_LEN_GRET);
        String longName = "A";
        for (int i = 2; i <= Customer.MAX_LN_LEN + 1; i++)
        {
            longName = longName + "a";
        }
        new Customer(okFirstName, longName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLastNameWithoutUppercaseLastLetter()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_RULES);
        String noUpperName = "johnson";
        new Customer(okFirstName, noUpperName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLastNameWithUppercaseLetters()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_RULES);
        String upperName = "PETERS";
        new Customer(okFirstName, upperName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLastNameWithDigits()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_RULES);
        String digitName = "Night007";
        new Customer(okFirstName, digitName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLastNameWithSymbols()
    {
        expectedEx.expect(InvalidLastNameCustomerException.class);
        expectedEx.expectMessage(InvalidLastNameCustomerException.LN_RULES);
        String symbolName = "Nickolson>_<";
        new Customer(okFirstName, symbolName, okLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithoutAt()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String loginWithoutAt = "jess.us";
        new Customer(okFirstName, okLastName, loginWithoutAt, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithAtInTheEnd()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String loginWithAtInTheEnd = "jess@";
        new Customer(okFirstName, okLastName, loginWithAtInTheEnd, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithSpaces()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String badLogin = "je ss@smwhr.com";
        new Customer(okFirstName, okLastName, badLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithoutHost()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String badLogin = "jess@.com";
        new Customer(okFirstName, okLastName, badLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithoutDomain()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String badLogin = "jess@smwhr";
        new Customer(okFirstName, okLastName, badLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLoginWithSymbols()
    {
        expectedEx.expect(InvalidEmailCustomerException.class);
        expectedEx.expectMessage(InvalidEmailCustomerException.MSG);
        String badLogin = "jess!@smwhr.com";
        new Customer(okFirstName, okLastName, badLogin, okPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithShortPassword()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_LEN_LESS);
        String shortPass = "SuN4*";
        new Customer(okFirstName, okLastName, okLogin, shortPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkShortPassword()
    {
        expectedEx = ExpectedException.none();
        String shortPass = "SuN4*r";
        new Customer(okFirstName, okLastName, okLogin, shortPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithOkLongPassword()
    {
        expectedEx = ExpectedException.none();
        String longPass = "AbCdefg456&*";
        new Customer(okFirstName, okLastName, okLogin, longPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithLongPassword()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_LEN_GRET);
        String longPass = "AbCdefg456&*#VALIDaTe1T";
        new Customer(okFirstName, okLastName, okLogin, longPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithoutUppercase()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_EASY);
        String easyPass = "password12%";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithoutLowercase()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_EASY);
        String easyPass = "PASSWORD12%";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithoutSymbols()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_EASY);
        String easyPass = "pasSword12";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithoutDigits()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_EASY);
        String easyPass = "pasSword%";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithLogin()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_MTCH_LOGIN);
        String easyPass = okLogin.substring(0, okLogin.indexOf("@")) + "1";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithFirstName()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_MTCH_FN);
        String easyPass = okFirstName + "1";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithPasswordWithLastName()
    {
        expectedEx.expect(InvalidPasswordCustomerException.class);
        expectedEx.expectMessage(InvalidPasswordCustomerException.PW_MTCH_LN);
        String easyPass = okLastName.toUpperCase() + "1";
        new Customer(okFirstName, okLastName, okLogin, easyPass, okMoney);
    }

    @Test
    public void testCreateNewCustomerWithZeroMoney()
    {
        expectedEx = ExpectedException.none();
        int money = 0;
        new Customer(okFirstName, okLastName, okLogin, okPass, money);
    }

    @Test
    public void testCreateNewCustomerWithNegativeMoney()
    {
        expectedEx.expect(InvalidMoneyCustomerException.class);
        expectedEx.expectMessage(InvalidMoneyCustomerException.MON_NEG);
        int money = -1;
        new Customer(okFirstName, okLastName, okLogin, okPass, money);
    }
}
