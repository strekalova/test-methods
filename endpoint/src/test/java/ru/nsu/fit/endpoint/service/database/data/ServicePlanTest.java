package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;

/**
 * Created by Владимир on 03.10.2016.
 */
public class ServicePlanTest {

    private String okName = "Alphavite123";
    private String okDetails = "Very_important_message_123_yep!!!][12398x.cnsaf,ma;,SDAJH";
    private int okMinSeats = 200;
    private int okMaxSeats = 500;
    private int okFeePerUnit = 19999;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewServicePlanWithShortName1()
    {
        expectedEx.expect(ServicePlanNameException.class);
        expectedEx.expectMessage(ServicePlanNameException.MIN_LEN);

        String name = "n";
        new ServicePlan(name, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithShortName2()
    {
        expectedEx = ExpectedException.none();

        String name = "na";
        new ServicePlan(name, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithLongName1()
    {
        expectedEx.expect(ServicePlanNameException.class);
        expectedEx.expectMessage(ServicePlanNameException.MAX_LEN);

        String name = "";
        for (int i= 0; i < 256; i++)
            name += "At";

        new ServicePlan(name, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithLongName2()
    {
        expectedEx =ExpectedException.none();

        String name = "";
        for (int i= 0; i < 64; i++)
            name += "At";

        new ServicePlan(name, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithSpecSymsName()
    {
        expectedEx.expect(ServicePlanNameException.class);
        expectedEx.expectMessage(ServicePlanNameException.SYMBOLS);

        String name = okName + "...";

        new ServicePlan(name, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithEmptyDetails()
    {
        expectedEx.expect(ServicePlanDetailsException.class);
        expectedEx.expectMessage(ServicePlanDetailsException.MIN_LEN);

        String det = "";

        new ServicePlan(okName, det, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithShortDetails()
    {
        expectedEx = ExpectedException.none();

        String det = "X";

        new ServicePlan(okName, det, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithLongDetails1()
    {
        expectedEx.expect(ServicePlanDetailsException.class);
        expectedEx.expectMessage(ServicePlanDetailsException.MAX_LEN);

        String det = "";
        for (int i = 0; i < 1024; i++)
            det += "At";

        new ServicePlan(okName, det, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithLongDetails2()
    {
        expectedEx = ExpectedException.none();

        String det = "";
        for (int i = 0; i < 512; i++)
            det += "At";

        new ServicePlan(okName, det, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithZeroMinSeats()
    {
        expectedEx.expect(ServicePlanMinSeatsException.class);
        expectedEx.expectMessage(ServicePlanMinSeatsException.MIN_VALUE);

        int mS = ServicePlan.minSeatsMin - 1;

        new ServicePlan(okName, okDetails, okMaxSeats, mS, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithOneMinSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.minSeatsMin;

        new ServicePlan(okName, okDetails, okMaxSeats, mS, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithOkBigMinSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.minSeatsMax;

        new ServicePlan(okName, okDetails, mS, mS, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithNotOkBigMinSeats()
    {
        expectedEx.expect(ServicePlanMinSeatsException.class);
        expectedEx.expectMessage(ServicePlanMinSeatsException.MAX_VALUE);

        int mS = ServicePlan.minSeatsMax + 1;

        new ServicePlan(okName, okDetails, okMaxSeats, mS, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithMinSeatsLessMaxSeats()
    {
        expectedEx = ExpectedException.none();
        new ServicePlan(okName, okDetails, okMaxSeats, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithMinSeatsGreaterMaxSeats()
    {
        expectedEx.expect(ServicePlanMinSeatsException.class);
        expectedEx.expectMessage(ServicePlanMinSeatsException.MAX_SEATS_CONSTRAINT);
        new ServicePlan(okName, okDetails, ServicePlan.minSeatsMin, ServicePlan.minSeatsMax, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithZeroMaxSeats()
    {
        expectedEx.expect(ServicePlanMaxSeatsException.class);
        expectedEx.expectMessage(ServicePlanMaxSeatsException.MIN_VALUE);

        int mS = ServicePlan.maxSeatsMin - 1;

        new ServicePlan(okName, okDetails, mS, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithOneMaxSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.maxSeatsMin;

        new ServicePlan(okName, okDetails, mS, mS, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithOkBigMaxSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.maxSeatsMax;

        new ServicePlan(okName, okDetails, mS, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithNotOkBigMaxSeats()
    {
        expectedEx.expect(ServicePlanMaxSeatsException.class);
        expectedEx.expectMessage(ServicePlanMaxSeatsException.MAX_VALUE);

        int mS = ServicePlan.maxSeatsMax + 1;

        new ServicePlan(okName, okDetails, mS, okMinSeats, okFeePerUnit);
    }

    @Test
    public void testCreateNewServicePlanWithNegativeFee()
    {
        expectedEx.expect(ServicePlanFeePerUnitException.class);
        expectedEx.expectMessage(ServicePlanFeePerUnitException.MIN_VALUE);

        int mS = ServicePlan.minFeePerUnit - 1;

        new ServicePlan(okName, okDetails, okMaxSeats, okMinSeats, mS);
    }

    @Test
    public void testCreateNewServicePlanWithZeroFee()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.minFeePerUnit;

        new ServicePlan(okName, okDetails, okMaxSeats, okMinSeats, mS);
    }

    @Test
    public void testCreateNewServicePlanWithBadMaxFee()
    {
        expectedEx.expect(ServicePlanFeePerUnitException.class);
        expectedEx.expectMessage(ServicePlanFeePerUnitException.MAX_VALUE);

        int mS = ServicePlan.maxFeePerUnit + 1;

        new ServicePlan(okName, okDetails, okMaxSeats, okMinSeats, mS);
    }

    @Test
    public void testCreateNewServicePlanWithMaxFee()
    {
        expectedEx = ExpectedException.none();

        int mS = ServicePlan.maxFeePerUnit;

        new ServicePlan(okName, okDetails, okMaxSeats, okMinSeats, mS);
    }
}
