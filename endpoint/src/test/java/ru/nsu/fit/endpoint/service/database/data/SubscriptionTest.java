package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;

/**
 * Created by Владимир on 03.10.2016.
 */
public class SubscriptionTest {

    private int okMinSeats = 200;
    private int okMaxSeats = 500;
    private int okUsedSeats = 300;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewSubscriptionWithZeroMinSeats()
    {
        expectedEx.expect(SubscriptionMinSeatsException.class);
        expectedEx.expectMessage(SubscriptionMinSeatsException.MIN_VALUE);

        int mS = Subscription.minSeatsMin - 1;

        new Subscription(okMaxSeats, mS, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithOneMinSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = Subscription.minSeatsMin;

        new Subscription(okMaxSeats, mS, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithOkBigMinSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = Subscription.minSeatsMax;

        new Subscription(mS, mS, mS);
    }

    @Test
    public void testCreateNewSubscriptionWithNotOkBigMinSeats()
    {
        expectedEx.expect(SubscriptionMinSeatsException.class);
        expectedEx.expectMessage(SubscriptionMinSeatsException.MAX_VALUE);

        int mS = Subscription.minSeatsMax + 1;

        new Subscription(okMaxSeats, mS, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithMinSeatsLessMaxSeats()
    {
        expectedEx = ExpectedException.none();
        new Subscription(okMaxSeats, okMinSeats, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithMinSeatsGreaterMaxSeats()
    {
        expectedEx.expect(SubscriptionMinSeatsException.class);
        expectedEx.expectMessage(SubscriptionMinSeatsException.MAX_SEATS_CONSTRAINT);
        new Subscription(Subscription.minSeatsMin, Subscription.minSeatsMax, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithZeroMaxSeats()
    {
        expectedEx.expect(SubscriptionMaxSeatsException.class);
        expectedEx.expectMessage(SubscriptionMaxSeatsException.MIN_VALUE);

        int mS = Subscription.maxSeatsMin - 1;

        new Subscription(mS, okMinSeats, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithOneMaxSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = Subscription.maxSeatsMin;

        new Subscription(mS, mS, mS);
    }

    @Test
    public void testCreateNewSubscriptionWithOkBigMaxSeats()
    {
        expectedEx = ExpectedException.none();

        int mS = Subscription.maxSeatsMax;

        new Subscription(mS, okMinSeats, okUsedSeats);
    }

    @Test
    public void testCreateNewSubscriptionWithNotOkUsedSeats() {
        expectedEx.expect(SubscriptionUsedSeatsException.class);
        expectedEx.expectMessage(SubscriptionUsedSeatsException.MIN_SEATS_CONSTRAINT);

        new Subscription(okMaxSeats, okMinSeats, okMinSeats - 1);
    }

    @Test
    public void testCreateNewSubscriptionWithNotOkBigMaxSeats() {
        expectedEx.expect(SubscriptionUsedSeatsException.class);
        expectedEx.expectMessage(SubscriptionUsedSeatsException.MAX_SEATS_CONSTRAINT);

        new Subscription(okMaxSeats, okMinSeats, okMaxSeats + 1);
    }
}
