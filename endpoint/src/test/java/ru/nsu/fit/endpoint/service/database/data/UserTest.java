package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;

/**
 * Created by Svetlana on 02.10.2016.
 */
public class UserTest
{
    private String okFirstName = "Carl";
    private String okLastName = "Lucas";
    private String okLogin = "luke_C@gmail.com";
    private String okPass = "missy4U@";

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewUserWithShortFirstName()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_LEN_LESS);
        String shortName = "A";
        for (int i = 2; i < User.MIN_FN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new User(shortName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithOkShortFirstName()
    {
        expectedEx = ExpectedException.none();
        String shortName = "A";
        for (int i = 2; i <= User.MIN_FN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new User(shortName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithOkLongFirstName()
    {
        expectedEx = ExpectedException.none();
        String longName = "A";
        for (int i = 2; i <= User.MAX_FN_LEN; i++)
        {
            longName = longName + "a";
        }
        new User(longName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLongFirstName()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_LEN_GRET);
        String longName = "A";
        for (int i = 2; i <= User.MAX_FN_LEN + 1; i++)
        {
            longName = longName + "a";
        }
        new User(longName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithFirstNameWithoutUppercaseFirstLetter()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_RULES);
        String noUpperName = "claire";
        new User(noUpperName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithFirstNameWithUppercaseLetters()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_RULES);
        String upperName = "PETER";
        new User(upperName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithFirstNameWithDigits()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_RULES);
        String digitName = "Nick007";
        new User(digitName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithFirstNameWithSymbols()
    {
        expectedEx.expect(InvalidFirstNameUserException.class);
        expectedEx.expectMessage(InvalidFirstNameUserException.FN_RULES);
        String symbolName = "Nicole^_^";
        new User(symbolName, okLastName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithShortLastName()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_LEN_LESS);
        String shortName = "A";
        for (int i = 2; i < User.MIN_LN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new User(okFirstName, shortName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithOkShortLastName()
    {
        expectedEx = ExpectedException.none();
        String shortName = "A";
        for (int i = 2; i <= User.MIN_LN_LEN; i++)
        {
            shortName = shortName + "a";
        }
        new User(okFirstName, shortName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithOkLongLastName()
    {
        expectedEx = ExpectedException.none();
        String longName = "A";
        for (int i = 2; i <= User.MAX_LN_LEN; i++)
        {
            longName = longName + "a";
        }
        new User(okFirstName, longName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLongLastName()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_LEN_GRET);
        String longName = "A";
        for (int i = 2; i <= User.MAX_LN_LEN + 1; i++)
        {
            longName = longName + "a";
        }
        new User(okFirstName, longName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLastNameWithoutUppercaseLastLetter()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_RULES);
        String noUpperName = "johnson";
        new User(okFirstName, noUpperName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLastNameWithUppercaseLetters()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_RULES);
        String upperName = "PETERS";
        new User(okFirstName, upperName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLastNameWithDigits()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_RULES);
        String digitName = "Night007";
        new User(okFirstName, digitName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLastNameWithSymbols()
    {
        expectedEx.expect(InvalidLastNameUserException.class);
        expectedEx.expectMessage(InvalidLastNameUserException.LN_RULES);
        String symbolName = "Nickolson>_<";
        new User(okFirstName, symbolName, okLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithoutAt()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String loginWithoutAt = "jess.us";
        new User(okFirstName, okLastName, loginWithoutAt, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithAtInTheEnd()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String loginWithAtInTheEnd = "jess@";
        new User(okFirstName, okLastName, loginWithAtInTheEnd, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithSpaces()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String badLogin = "je ss@smwhr.com";
        new User(okFirstName, okLastName, badLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithoutHost()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String badLogin = "jess@.com";
        new User(okFirstName, okLastName, badLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithoutDomain()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String badLogin = "jess@smwhr";
        new User(okFirstName, okLastName, badLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithLoginWithSymbols()
    {
        expectedEx.expect(InvalidEmailUserException.class);
        expectedEx.expectMessage(InvalidEmailUserException.MSG);
        String badLogin = "jess!@smwhr.com";
        new User(okFirstName, okLastName, badLogin, okPass);
    }

    @Test
    public void testCreateNewUserWithShortPassword()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_LEN_LESS);
        String shortPass = "SuN4*";
        new User(okFirstName, okLastName, okLogin, shortPass);
    }

    @Test
    public void testCreateNewUserWithOkShortPassword()
    {
        expectedEx = ExpectedException.none();
        String shortPass = "SuN4*r";
        new User(okFirstName, okLastName, okLogin, shortPass);
    }

    @Test
    public void testCreateNewUserWithOkLongPassword()
    {
        expectedEx = ExpectedException.none();
        String longPass = "AbCdefg456&*";
        new User(okFirstName, okLastName, okLogin, longPass);
    }

    @Test
    public void testCreateNewUserWithLongPassword()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_LEN_GRET);
        String longPass = "AbCdefg456&*#VALIDaTe1T";
        new User(okFirstName, okLastName, okLogin, longPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithoutUppercase()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_EASY);
        String easyPass = "password12%";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithoutLowercase()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_EASY);
        String easyPass = "PASSWORD12%";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithoutSymbols()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_EASY);
        String easyPass = "pasSword12";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithoutDigits()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_EASY);
        String easyPass = "pasSword%";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithLogin()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_MTCH_LOGIN);
        String easyPass = okLogin.substring(0, okLogin.indexOf("@")) + "1";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithFirstName()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_MTCH_FN);
        String easyPass = okFirstName + "123";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }

    @Test
    public void testCreateNewUserWithPasswordWithLastName()
    {
        expectedEx.expect(InvalidPasswordUserException.class);
        expectedEx.expectMessage(InvalidPasswordUserException.PW_MTCH_LN);
        String easyPass = okLastName.toUpperCase() + "1";
        new User(okFirstName, okLastName, okLogin, easyPass);
    }
}
